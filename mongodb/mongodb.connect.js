const mongoose = require('mongoose');

async function connect() {
  try {
    await mongoose.connect(
      'mongodb+srv://user:nfEXE-97858cpj*@cluster0.ukabk.mongodb.net/todo-td?authSource=admin',
      // 'mongodb://localhost:27017'
      { useNewUrlParser: true, useUnifiedTopology: true }
    );
  } catch (err) {
    console.error('Error connecting to mongodb');
    console.error(err);
  }
}

module.exports = { connect };
